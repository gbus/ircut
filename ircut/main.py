import click
import RPi.GPIO as GPIO
import logging
from time import sleep


log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)

log.addHandler(ch)

settings = {
    "in1": 16,  # GPIO 23
    "in2": 18,  # GPIO 24
    "en": 22,   # GPIO 25
    "inverted_output": True,    # Set to True if behavior is inverted
}

GPIO.setmode(GPIO.BOARD)
log.debug("GPIO mode set to BOARD")


def init_gpio(inputs: dict) -> None:
    """
    Init GPIO pins for output
    :param inputs: GPIO pins
    :return: None
    """

    GPIO.setup(inputs["in1"], GPIO.OUT)
    GPIO.setup(inputs["in2"], GPIO.OUT)
    GPIO.setup(inputs["en"], GPIO.OUT)
    log.debug("Set pins %d, %d and %d as output", inputs["in1"], inputs["in2"], inputs["en"])


def set_inputs_high_low(in_high: int, in_low: int) -> None:
    """
    Set one GPIO high, the other low
    :param in_high: Input to set high
    :param in_low: Input to set low
    :return: None
    """
    GPIO.output(in_high, GPIO.HIGH)
    GPIO.output(in_low, GPIO.LOW)
    log.info("Set pin %d to HIGH and pin %d to LOW", in_high, in_low)


def activate_filter(settings: dict, activate: bool) -> None:
    """
    Activate/deactivate IR filter
    :param settings: GPIO pins
    :param activate: activate/deactivate command
    :return: None
    """
    if activate ^ (not settings["inverted_output"]):
        set_inputs_high_low(settings["in1"], settings["in2"])
    else:
        set_inputs_high_low(settings["in2"], settings["in1"])
    GPIO.output(settings["en"], GPIO.HIGH)
    log.info("Set enable pin %d to HIGH", settings["en"])
    sleep(1)


def reset_gpio(enable_pin: int) -> None:
    """
    Reset enable pin and GPIO cleanup
    :param enable_pin: enable pin
    :return: None
    """
    GPIO.output(enable_pin, GPIO.LOW)
    GPIO.cleanup()


@click.command()
@click.option('--filter/--no-filter', required=True, type=bool, default=False, help='Enable/disable IR filter.')
def set_ircut(filter):
    """Set ircut to on or off"""
    init_gpio(settings)
    activate_filter(settings, filter)
    click.echo(f"Setting night mode to {filter ^ (not settings['inverted_output'])}")

    reset_gpio(settings['en'])
