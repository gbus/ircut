from setuptools import setup, find_packages


setup(
    name="ircut",
    version="0.1.0",
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "Click",
        "RPi.GPIO"
    ],
    entry_points={
        'console_scripts': [
            'ircut = ircut.main:set_ircut',
        ],
    },
)